from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import sys


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import pytest

        errno = pytest.main(self.test_args)
        sys.exit(errno)


setup(
    name="tweetaway",
    version="0.1-alpha",
    packages=find_packages(),

    install_requires=['pyramid==1.4.5',
                      'pyramid_debugtoolbar',
                      'waitress',
                      'SQLAlchemy',
                      'zope.sqlalchemy',
                      'transaction',
                      'pyramid_tm',
                      'birdy',
                      'psycopg2',
                      'passlib',
                      'APScheduler',
    ],

    package_data={
    },

    # metadata for upload to PyPI
    author="Konrad Strack, Michal Drzal",
    author_email="",
    description="A small project built with Pyramid.",
    license="MIT",
    keywords="pyramid twitter",
    url="https://bitbucket.org/konradstrack/studies-pyramid-twitter-app", # project home page

    entry_points="""\
        [paste.app_factory]
        main = tweetaway:main
        [console_scripts]
        initialize_tweetaway_db = tweetaway.scripts.initializedb:main
    """,

    tests_require=['pytest', 'pytest-cov'],
    cmdclass={'test': PyTest},
)
