"""This module is responsible for initializing the database. \
It is installed by setup.py and made available as a script that can \
be invoked manually when creation of the database schema or populating \
tha database with data is required.
"""

import os
import sys
import transaction
from datetime import datetime

from sqlalchemy import engine_from_config

from pyramid.paster import get_appsettings, setup_logging

from ..models import DBSession, Base
from ..models import Tweet, User
from tweetaway.models import Role


def usage(argv):
    """Prints out the usage of the initialize db script.
    """
    cmd = os.path.basename(argv[0])
    print('usage: %s <config_uri> [sqlalchemy_url]\n'
          '(example: "%s development.ini sqlite:///development.sqlite")' % (cmd, cmd))
    sys.exit(1)


def main(argv=sys.argv):
    """Reads settings, creates the database schema and populates the database with example data.
    """
    if len(argv) == 2:
        config_uri = argv[1]
        settings = get_appsettings(config_uri)
    elif len(argv) == 3:
        config_uri = argv[1]
        # special handling for the Heroku PostgreSQL URL
        settings = {'sqlalchemy.url': argv[2]}
    else:
        usage(argv)

    setup_logging(config_uri)

    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.create_all(engine)

    with transaction.manager:
        role = Role(name='admin')
        DBSession.add(role)

        admin1 = User(login='konradstrack', password='test1', last_login=datetime.now())
        admin1.roles = [role]
        DBSession.add(admin1)

        admin2 = User(login='michaldrzal', password='test2', last_login=datetime.now())
        admin2.roles = [role]
        DBSession.add(admin2)

