"""This package contains scripts used to perform various application-related tasks, \
such as initialization of the database schema.
"""

__author__ = 'Konrad Strack'
