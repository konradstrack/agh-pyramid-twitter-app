"""This module defines all models used in the application.
"""
from passlib.handlers.sha2_crypt import sha512_crypt

from sqlalchemy import Column, Integer, Text, DateTime, Unicode, ForeignKey

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker, relationship
from sqlalchemy.schema import PrimaryKeyConstraint

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()


def hash_password(password):
    """Create a hash of the password with SHA-512."""

    return sha512_crypt.encrypt(password)


def verify_password(password, password_hash):
    """Verifies if the provided password is correct."""

    return sha512_crypt.verify(password, password_hash)


class AdminSettings(Base):
    """
    Used to store settings as a key-value store.
    """
    __tablename__ = 'admin_settings'
    key = Column(Text, primary_key=True)
    value = Column(Text)

    def __init__(self, key, value):
        self.key = key
        self.value = value


class Friend(Base):
    """
    Represents a single user that is being followed.
    """

    __tablename__ = 'friends'
    id = Column(Integer, primary_key=True)
    screen_name = Column(Text)
    img_url = Column(Text)

    def __init__(self, screen_name, img_url):
        self.screen_name = screen_name
        self.img_url = img_url


class UserFriend(Base):
    """Mapping between users and their selected friends."""

    __tablename__ = 'user_friends'
    user_id = Column(Integer, ForeignKey('users.id'))
    friend_id = Column(Integer, ForeignKey('friends.id'))

    __table_args__ = (PrimaryKeyConstraint(user_id, friend_id),)


class Tweet(Base):
    """
    Represents a single Tweet.
    """

    __tablename__ = 'tweets'
    id = Column(Integer, primary_key=True)
    tweet_id = Column(Integer)
    screen_name = Column(Text)
    text = Column(Text)
    createdAt = Column(DateTime)

    def __init__(self, tweet_id, screen_name, text, createdAt):
        self.tweet_id = tweet_id
        self.screen_name = screen_name
        self.text = text
        self.createdAt = createdAt


class UserRole(Base):
    """Mapping between users and their roles."""

    __tablename__ = 'user_roles'
    user_id = Column(Integer, ForeignKey('users.id'))
    role_id = Column(Integer, ForeignKey('roles.id'))

    __table_args__ = (PrimaryKeyConstraint(user_id, role_id),)


class User(Base):
    """Represents a user in the application."""

    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    login = Column(Text, unique=True, index=True)
    last_login = Column(DateTime)

    roles = relationship('Role', secondary='user_roles', backref='users')
    friends = relationship('Friend', secondary='user_friends', backref='users')

    _password = Column('password', Text)

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, password):
        self._password = hash_password(password)

    def verify_password(self, plaintext_password):
        return verify_password(plaintext_password, self.password)

    def __init__(self, login, password, last_login):
        self.login = login
        self.password = password
        self.last_login = last_login


class Role(Base):
    """Represents a role in the application. Roles can be assigned to users
     through the association table."""

    __tablename__ = 'roles'
    id = Column(Integer, primary_key=True)
    name = Column(Text)





