<%inherit file="../layout.mak"/>

<%block name="navigation">
</%block>
<%block name="main">
    <p>Please edit your credentials for your dev.twitter.com account:</p>
    <form name="input" action="/loaded" method="post">
		<table>
			<tr>
			  <td align="right">Consumer key</td>
			  <td align="left"><input type="text" name="a1" value="${consumer_key}"/></td>
			</tr>
			<tr>
			  <td align="right">Consumer secret</td>
			  <td align="left"><input type="text" name="a2" value="${consumer_secret}"/></td>
			</tr>
			<tr>
			  <td align="right">Access token</td>
			  <td align="left"><input type="text" name="b1" value="${access_token}"/></td>
			</tr>
			<tr>
			  <td align="right">Access token secret</td>
			  <td align="left"><input type="text" name="b2" value="${access_token_secret}"/></td>
			</tr>
			<tr>
			  <td align="left"><input type="submit" value="Submit"></td>
			</tr>
		</table>
	</form>
</%block>

