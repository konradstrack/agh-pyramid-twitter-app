<%inherit file="../layout.mak"/>

<%block name="navigation">
</%block>

<%block name="main">
	<p>${message}</p>
    <p>Friends list:</p>
    <ul>
		% for a in user_names:
			<li>${a[0]} <a href="/remove_friend?user_name=${a[0]}">remove</a></li> <img src="${a[1]}"></img>
		% endfor
	</ul>
	<form name="input" action="/friends" method="post">
		<table>
			<tr>
			  <td align="right">New friend name</td>
			  <td align="left"><input type="text" name="friend_name" /></td>
			</tr>
			<tr>
			  <td align="left"><input type="submit" value="Add friend"></td>
			</tr>
		</table>
	</form>
	
	<p>Back to the <a href="admin_panel">admin panel</a><p>
</%block>

