<%inherit file="../layout.mak"/>

<%block name="navigation">
</%block>

<%block name="main">
	<h2>Administration panel</h2>

    <p>Here's what you can do:
	<ul>
        <li>Update Twitter API <a href="update_credentials">credentials</a>.</li>
	    <li>Manage <a href="friends">friends</a> of the account.</li>
        <li><a href="/enable_tweet_loading">Enable</a> or <a href="/disable_tweet_loading">disable</a> updating local database from Twitter.</li>
        <li>View your <a href="user_panel">user panel</a>.</li>
    </ul>
</%block>
