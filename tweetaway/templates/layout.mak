<html>
    <head>
        <link rel="stylesheet" href="/static/stylesheets/layout.css"/>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700,300&subset=latin,latin-ext'
              rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,700,200,300&subset=latin,latin-ext'
              rel='stylesheet' type='text/css'/>
    </head>

    <body>
        <div id="container">
            <aside id="left">
							<a href="${request.resource_url(request.context, '/')}"><h1>Tweetaway</h1></a>
            </aside>

            <aside id="navigation">
                <%block name="navigation"/>
                % if user:
                    <a href="${request.resource_url(request.context, 'user_panel')}">Panel</a><br />
                    <a href="${request.resource_url(request.context, 'new_tweets')}">New tweets</a><br />
                    <a href="${request.resource_url(request.context, 'older_tweets')}">Old tweets</a>
                % endif
            </aside>

            <div id="right">
                <nav id="header">
                    % if user is None:
                        <a href="${request.resource_url(request.context, 'login')}">Log in</a>
                        <a href="${request.resource_url(request.context, 'signup')}">Sign up</a>
                    % else:
                        <span class="logged-in">Logged in as: ${user}</span>
                        <a href="${request.resource_url(request.context, 'logout')}">Log out</a>
                    % endif
                </nav>

                <div id="main">
                    <%block name="main"/>
                </div>
            </div>
        </div>
    </body>
</html>
