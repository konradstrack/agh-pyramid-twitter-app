<%inherit file="../layout.mak"/>

<%block name="navigation">
</%block>

<%block name="main">
    <h2>Sign up</h2>

    <p>${message}</p>

    <form action="${url}" method="post">
        <label for="login">Choose your login</label>
        <input type="text" id="login" name="login" value="${login}"/>

        <label for="password">Password</label>
        <input type="password" id="password" name="password"/>

        <label for="repeat_password">Repeat password</label>
        <input type="password" id="repeat_password" name="repeat_password"/>

        <input type="submit" name="form.submitted" value="Sign up"/>
    </form>
</%block>
