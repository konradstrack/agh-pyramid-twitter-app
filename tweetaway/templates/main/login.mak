<%inherit file="../layout.mak"/>

<%block name="navigation">
</%block>

<%block name="main">
    <h2>Log in</h2>

    <p>${message}</p>

    <form action="${url}" method="post">
        <input type="hidden" name="came_from" value="${came_from}"/>

        <label for="login">Login</label>
        <input type="text" id="login" name="login" value="${login}"/>

        <label for="password">Password</label>
        <input type="password" id="password" name="password" value="${password}"/>

        <input type="submit" name="form.submitted" value="Log In"/>
    </form>
</%block>
