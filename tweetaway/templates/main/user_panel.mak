<%inherit file="../layout.mak"/>

<%block name="navigation">
</%block>

<%block name="main">
    <h2>Welcome <em>${user}</em>!</h2>
	<p>
        Here's what you can do:
        <ul>
            <li><a href="/user_friends">Add friends</a></li>
            <li><a href="/new_tweets">View new tweets</a></li>
            <li><a href="/older_tweets">View older tweets</a></li>
        </ul>
	</p>
</%block>
