<%inherit file="../layout.mak"/>

<%block name="navigation">
</%block>

<%block name="main">
	<p>${message}</p>

	<h2>Friends</h2>
    % if not friends:
        <p>No friends available yet? Contact your administrator.</p>
    % else:
        <p>Select friends you want to follow:</p>
        <form action="/user_friends" method="post">
        <ul>
            % for a in friends:
                <li> ${a[0]} <input type="checkbox" name="name="selected_friends"" ${a[1]} value="${a[0]}"></li>
            % endfor
        </ul>
    	<input type="submit" value="Change"/>
	    </form>
    % endif

	<p>Go back to <a href="user_panel">user panel</a>.<p>
</%block>
