<%inherit file="../layout.mak"/>

<%block name="navigation">
</%block>

<%block name="main">
	<p>${message}</p>
    <h2>New tweets</h2>

	<ul class="tweets">
    % for tweet in tweets:
        <li>
            <span class="tweet-text">${tweet.text}</span>
            <span class="tweet-screen-name">${tweet.screen_name}</span>
        </li>
    % endfor
    </ul>
	
</%block>
