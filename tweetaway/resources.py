from pyramid.security import Allow, Everyone, Authenticated


class Root(dict):
    __name__ = ''
    __parent__ = None

    __acl__ = [
        (Allow, Authenticated, 'view'),
        (Allow, 'r:admin', 'administer'),
    ]

    def __init__(self, request):
        self.request = request
