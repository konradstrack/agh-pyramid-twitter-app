"""This module serves as an entry point to the application \
and it defines its core configuration.
"""
from pyramid.authentication import AuthTktAuthenticationPolicy
from pyramid.authorization import ACLAuthorizationPolicy

from pyramid.config import Configurator
from pyramid.events import BeforeRender
from pyramid.security import authenticated_userid

from sqlalchemy import engine_from_config
from .models import DBSession, Base
from tweetaway.resources import Root
from tweetaway.security import auth_tkt_secret, find_roles

from .scheduler import sched


def add_renderer_globals(event):
   event['user'] = authenticated_userid(event['request'])

def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    This is the main entry point to the Tweetaway application. All routes are defined here.
    """

    # configuration for sqlalchemy
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine

    sched.start()

    # authentication and authorization policies
    authentication_policy = AuthTktAuthenticationPolicy(
        secret=auth_tkt_secret,
        callback=find_roles,
        hashalg='sha512'
    )
    authorization_policy = ACLAuthorizationPolicy()

    # creation of the configurator
    config = Configurator(settings=settings, root_factory=Root)

    config.set_authentication_policy(authentication_policy)
    config.set_authorization_policy(authorization_policy)

    config.add_static_view(name='static', path='static')

    config.add_route('home', '/')
    config.add_route('login', '/login')
    config.add_route('logout', '/logout')
    config.add_route('signup', '/signup')

    config.add_route('update_credentials', '/update_credentials')
    config.add_route('update_tweets', '/loaded')
    config.add_route('tweet_stream', '/stream')
    config.add_route('admin_panel', '/admin_panel')
    config.add_route('user_panel', '/user_panel')
    config.add_route('friends', '/friends')
    config.add_route('remove_friend', '/remove_friend')
    config.add_route('disable_tweet_loading', '/disable_tweet_loading')
    config.add_route('enable_tweet_loading', '/enable_tweet_loading')
    config.add_route('user_friends', '/user_friends')
    config.add_route('new_tweets', '/new_tweets')
    config.add_route('older_tweets', '/older_tweets')
    
    config.scan()

    config.add_subscriber(add_renderer_globals, BeforeRender)

    return config.make_wsgi_app()
