from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from birdy.twitter import UserClient
import transaction

from ..models import DBSession, Tweet, AdminSettings, Friend
from sqlalchemy import func

from urllib.parse import parse_qs
from ..scheduler import sched
from tweetaway.helpers import convert_time


@view_config(route_name='admin_panel', request_method='GET', renderer='admin/admin_panel.mak',
             permission='administer')
def admin_panel(request):
    """Home page of the application."""

    return {}


@view_config(route_name='friends', renderer='admin/friends.mak',
             permission='administer')
def friends(request):
    """Management of friends."""
    
    
    friends = DBSession.query(Friend).all()
    
    
                
    dic = {x.key: x.value for x in DBSession.query(AdminSettings).all()}

    if len(dic) < 4:
        return HTTPFound(location="/update_credentials")

    client = UserClient(dic["consumer_key"], dic["consumer_secret"], dic["access_token"], dic["access_token_secret"])

    user_names = [(user.screen_name, user.img_url) for user in friends]

    message = ""
    if request.method == "POST":
        data = request.POST
        friend_name = data["friend_name"]

        print(friend_name)
        data = None
        if len([x for x in friends if x.screen_name == friend_name]) == 0:
            try:
                resp = client.api.friendships.create.post(screen_name=friend_name)
                data = resp.data
            except:
                return {"user_names": user_names, "message": "No such user name"}
            finally:
                if data is not None:
                    DBSession.add(Friend(data["screen_name"], data["profile_image_url_https"]))
                    return HTTPFound(location="/friends")
                else:
                    return {"user_names": user_names, "message": "No such user name or wrong credentials"}

    return {"user_names": user_names, "message": message}


@view_config(route_name='enable_tweet_loading', renderer='admin/enable_tweet_loading.mak',
             permission='administer')
def enable_tweet_loading(request):
    """Enables fetching of Tweets."""

    def timed_job():
        friends = DBSession.query(Friend).all()
        dic = {x.key: x.value for x in DBSession.query(AdminSettings).all()}
        if len(dic) < 4:
            return

        client = UserClient(dic["consumer_key"], dic["consumer_secret"], dic["access_token"],
                            dic["access_token_secret"])

        for friend in friends:
            last_tweet = DBSession.query(Tweet.tweet_id) \
                .filter(Tweet.screen_name == friend.screen_name).all()
            resp = None
            last_tweet = [tweet[0] for tweet in last_tweet]
            

            if last_tweet != []:
                print(max(last_tweet))
                resp = client.api.statuses.user_timeline.get(screen_name=friend.screen_name,
                                                             since_id=max(last_tweet))
            else:
                print("no last tweet")
                resp = client.api.statuses.user_timeline.get(screen_name=friend.screen_name)

            tweets = [Tweet(tweet['id'], friend.screen_name, tweet['text'], convert_time(tweet['created_at'])) for tweet
                      in resp.data]

            for tweet in tweets:
                print(tweet.tweet_id)

            with transaction.manager:
                DBSession.add_all(tweets)


    jobs = sched.get_jobs()
    if len(jobs) < 1:
        sched.add_interval_job(timed_job, seconds=20)
        return {"result": "Tweets are now synced from Twitter account"}
    else:
        return {"result": "Tweets are already being synced"}


@view_config(route_name='disable_tweet_loading', renderer='admin/disable_tweet_loading.mak',
             permission='administer')
def disable_tweet_loading(request):
    """Disables fetching of Tweets."""
    jobs = sched.get_jobs()
    if len(jobs) < 1:
        return {"result": "Tweets already are not being synced from Twitter account"}
    else:
        sched.unschedule_job(jobs[0])
        return {"result": "You disabled syncing tweets from the Twitter account"}


@view_config(route_name='remove_friend', renderer='admin/remove_friend.mak',
             permission='administer')
def remove_friend(request):
    """Removes a friend from the friend list."""
    qs = parse_qs(request.query_string)
    dic = {x.key: x.value for x in DBSession.query(AdminSettings).all()}

    if len(dic) == 0:
        return HTTPFound(location="/update_credentials")

    item = DBSession.query(Friend).filter(Friend.screen_name == qs["user_name"][0]).first()
    if item is None:
        return {"message": "No such user: " + qs["user_name"][0]}

    client = UserClient(dic["consumer_key"], dic["consumer_secret"], dic["access_token"], dic["access_token_secret"])
    resp = client.api.friendships.destroy.post(screen_name=qs["user_name"][0])

    if resp.data is None:
        return HTTPFound(location="/update_credentials")
    else:
        DBSession.delete(item)
        return {"message": "User deleted: " + qs["user_name"][0]}


@view_config(route_name='update_credentials', request_method='GET', renderer='admin/load_tweets.mak',
             permission='administer')
def update_credentials(request):
    """Displays a form allowing for editing user credentials required to administer account.
    """
    dic = {x.key: x.value for x in DBSession.query(AdminSettings).all()}

    if len(dic) == 0:
        return {"consumer_key": "", "consumer_secret": "", "access_token": "", "access_token_secret": ""}
    return dic


@view_config(route_name='update_tweets', renderer='admin/update_tweets.mak',
             permission='administer')
def update_tweets(request):
    """Adds list of user friends tweets to the database. \
    No checks performed so far to prevent creating doubled entries for each tweet.
    """
    if request.method == "POST":
        data = request.POST
        consumer_key = data["a1"]
        consumer_secret = data["a2"]
        access_token = data["b1"]
        access_token_secret = data["b2"]

        with transaction.manager:
            DBSession.merge(AdminSettings(key='consumer_key', value=consumer_key))
            DBSession.merge(AdminSettings(key='consumer_secret', value=consumer_secret))
            DBSession.merge(AdminSettings(key='access_token', value=access_token))
            DBSession.merge(AdminSettings(key='access_token_secret', value=access_token_secret))

        return {"text": "Admin credentials updated"}
    else:
        return {"text": ""}
