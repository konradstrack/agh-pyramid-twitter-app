from pyramid.httpexceptions import HTTPFound
from pyramid.security import remember, forget, authenticated_userid
from pyramid.view import view_config, forbidden_view_config
import transaction
from ..models import DBSession, Tweet, Friend
from tweetaway.helpers import find_friends
from tweetaway.models import User
from tweetaway.security import find_roles


@view_config(route_name='home', request_method='GET', renderer='main/home.mak')
def home(request):
    """Home page of the application.
    """
    roles = find_roles(authenticated_userid(request), request)

    if roles is None:
        return {}
    elif "r:admin" in roles:
        return HTTPFound(location="admin_panel")
    else:
        return HTTPFound(location="user_panel")

    return {}


@view_config(route_name='user_panel', request_method='GET', renderer='main/user_panel.mak',
             permission='view')
def user_panel(request):
    """Home page of the application.
    """

    return {}


@view_config(route_name='user_friends', renderer='main/user_friends.mak',
             permission='view')
def user_friends(request):
    """Home page of the application.
    """
    message = ""
    if request.method == "POST":
        data = request.POST
        screen_names = []
        for key, value in data.iteritems():
            screen_names.append(value)

        new_friends = DBSession.query(Friend).filter(Friend.screen_name.in_(tuple(screen_names))).all()

        user = DBSession.query(User).filter_by(login=authenticated_userid(request)).first()
        user.friends = new_friends
        DBSession.merge(user)

        message = "Friends list edited"

    user_friends = set(find_friends(authenticated_userid(request)))
    all_friends = [(x.screen_name, "checked" if x.screen_name in user_friends else "") for x in
                   DBSession.query(Friend).all()]

    return {"friends": all_friends, "message": message}


@view_config(route_name='new_tweets', renderer='main/new_tweets.mak',
             permission='view')
def new_tweets(request):
    """Home page of the application.
    """
    message = ""
    user = DBSession.query(User).filter_by(login=authenticated_userid(request)).first()
    screen_names = [x.screen_name for x in user.friends]
    tweets = DBSession.query(Tweet).filter(Tweet.screen_name.in_(tuple(screen_names))).filter(
        Tweet.createdAt >= user.last_login).all()

    return {"tweets": tweets, "message": message}


@view_config(route_name='older_tweets', renderer='main/older_tweets.mak',
             permission='view')
def older_tweets(request):
    """Home page of the application.
    """
    message = ""
    user = DBSession.query(User).filter_by(login=authenticated_userid(request)).first()
    screen_names = [x.screen_name for x in user.friends]
    tweets = DBSession.query(Tweet).filter(Tweet.screen_name.in_(tuple(screen_names))).filter(
        Tweet.createdAt < user.last_login).all()

    return {"tweets": tweets, "message": message}


@view_config(route_name='login', renderer='main/login.mak')
@forbidden_view_config(renderer='main/login.mak')
def login(request):
    login_url = request.resource_url(request.context, 'login')
    referrer = request.url
    if referrer == login_url:
        referrer = '/' # never use the login form itself as came_from
    came_from = request.params.get('came_from', referrer)

    message = ''
    login = ''
    password = ''

    if 'form.submitted' in request.params:
        login = request.params['login']
        password = request.params['password']
        user = DBSession.query(User).filter(User.login == login).first()
        if user and user.verify_password(password):
            headers = remember(request, login)
            return HTTPFound(location=came_from, headers=headers)

        message = 'Failed login.'

    return dict(
        message=message,
        came_from=came_from,
        url=login_url,
        login=login,
        password=password,
    )


@view_config(route_name='logout')
def logout(request):
    headers = forget(request)
    return HTTPFound(location=request.resource_url(request.context), headers=headers)


@view_config(route_name='signup', renderer='main/signup.mak')
def signup(request):
    signup_url = request.resource_url(request.context, 'signup')
    message = ''
    login = ''

    if 'form.submitted' in request.params:
        login = request.params['login']
        password = request.params['password']
        repeat_password = request.params['repeat_password']

        user = DBSession.query(User).filter(User.login == login).first()
        if user:
            message = 'This name has already been taken.'
        elif password == repeat_password:
            with transaction.manager:
                user = User(login=login, password=password, last_login=None)
                DBSession.add(user)

            return HTTPFound(request.resource_url(request.context, 'login'))
        else:
            message = 'Passwords do not match.'

    return dict(
        message=message,
        url=signup_url,
        login=login,
    )


@view_config(route_name='tweet_stream', request_method='GET', renderer='main/tweet_stream.mak',
             permission='view')
def tweet_stream(request):
    """Displays a stream containing all Tweets.
    """
    tweets = DBSession.query(Tweet).all()
    return {'tweets': tweets}
               
    

    
    

