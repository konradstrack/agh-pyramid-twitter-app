from tweetaway.models import DBSession, User

auth_tkt_secret = '4PVn2NDKbN3ZziJHBvWjxMObgEGnNvWb'


def find_roles(login, request):
    user = DBSession.query(User).filter_by(login=login).first()
    if user is not None:
        return ['r:{0}'.format(role.name) for role in user.roles]
    else:
        return None
