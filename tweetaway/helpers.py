from tweetaway.models import DBSession, User
from time import mktime, strptime
from datetime import datetime


def convert_time(twitter_date):
    return datetime.fromtimestamp(mktime(strptime(twitter_date, '%a %b %d %H:%M:%S +0000 %Y')))


def find_friends(login):
    user = DBSession.query(User).filter_by(login=login).first()
    if user is not None:
        return [friend.screen_name for friend in user.friends]
    else:
        return None
