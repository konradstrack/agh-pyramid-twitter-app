import tweetaway.models as models

from unittest.mock import Mock


class TestPasswordHandling(object):
    def test_if_password_is_hashed(self):
        models.hash_password = Mock(return_value="hashed")

        user = models.User(login='someuser', password='somepassword', last_login=None)
        user.password = 'otherpassword'

        models.hash_password.assert_called_with('otherpassword')

    def test_if_password_is_verified(self):
        models.verify_password = Mock()
        models.hash_password = Mock(return_value='hashed')

        user = models.User(login='someuser', password='somepassword', last_login=None)
        user.password = 'otherpassword'
        user.verify_password('wrongpassword')

        models.verify_password.assert_called_with('wrongpassword', 'hashed')