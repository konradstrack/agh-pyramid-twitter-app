from datetime import datetime
from tweetaway.security import find_roles
from tweetaway.models import DBSession, Base, Role, User

from sqlalchemy import create_engine


class TestSecurity(object):
    @classmethod
    def setup_class(cls):
        engine = create_engine('sqlite:///:memory:')
        DBSession.configure(bind=engine)

        Base.metadata.create_all(engine)

        admin = User(login='administrator', password='somepassword', last_login=datetime.now())
        DBSession.add(admin)

    def test_if_returns_correct_roles(self):
        roles = find_roles('administrator', None)
        assert len(roles) == 0

        # add some roles
        role1 = Role(name='admin')
        role2 = Role(name='any')

        DBSession.add(role1)
        DBSession.add(role2)

        admin = DBSession.query(User).filter(User.login == 'administrator').one()
        admin.roles = [role1, role2]

        roles = find_roles('administrator', None)

        assert 'r:admin' in roles
        assert 'r:any' in roles
        assert len(roles) == 2

    def test_if_fails_to_verify_wrong_password(self):
        admin = DBSession.query(User).filter(User.login == 'administrator').one()
        admin.password = 'correctpassword'
        assert admin.verify_password('wrongpassword') is False

    def test_if_accepts_correct_password(self):
        admin = DBSession.query(User).filter(User.login == 'administrator').one()
        admin.password = 'correctpassword'
        assert admin.verify_password('correctpassword')