from datetime import datetime
from tweetaway.helpers import find_friends
from tweetaway.security import find_roles
from tweetaway.models import DBSession, Base, Role, User, Friend

from sqlalchemy import create_engine


class TestSecurity(object):
    @classmethod
    def setup_class(cls):
        engine = create_engine('sqlite:///:memory:')
        DBSession.configure(bind=engine)

        Base.metadata.create_all(engine)

    def test_if_returns_correct_friends(self):
        user = User(login='someuser', password='somepassword', last_login=datetime.now())

        friend1 = Friend(screen_name="friend1", img_url="url1")
        friend2 = Friend(screen_name="friend2", img_url="url2")

        user.friends = [friend1, friend2]
        DBSession.add(user)

        friends = find_friends('someuser')
        assert "friend1" in friends
        assert "friend2" in friends
        assert len(friends) == 2